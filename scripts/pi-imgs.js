function randomNumber(min, max) {
    return Math.floor(Math.random() * (max - min) + min);
}

function getRandomImage() {
    path = 'chitra/pi/'
    var num = randomNumber(1, 9);
    var img;
	if (num === 8) {
		img = path + num + '.gif'; // Use .gif for number 8
	} else {
		img = path + num + '.webp'; // Use .webp for other numbers
	}
    return img;
}

document.addEventListener('DOMContentLoaded', (event) => {
    var gal = document.getElementById('pi-gallery');
    for(var i = 0; i < 1; i++){
	var img = document.createElement("img");
	img.src = getRandomImage();
	gal.appendChild(img);
    }
});
