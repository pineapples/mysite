$('#leftarrow').click(function(){
	$('#search').animate({ 'right': '0px' });
	$('#leftarrow').css({ 'display': 'none' });
	$('#rightarrow').css({ 'display': 'inline-block' });
});

$('#rightarrow').click(function(){
	$('#search').animate({ 'right': '-225px' });
	$('#leftarrow').css({ 'display': 'inline-block' });
	$('#rightarrow').css({ 'display': 'none' });
});

$('#searchfield').keyup(function(){
	search();
});

$('#searchbutton').click(function(){
	search();
});

if($('#searchfield').val() == ''){
	$('article').css({ 'display': 'block' });
}

function search(){
	var searchquery = document.getElementById('searchfield').value.toLowerCase();
	$('article').css({ 'display': 'block' });
	$('article').filter(function () {
		return ($(this).find('*').text().toLowerCase().indexOf(searchquery) == -1)
	}).css({ 'display': 'none' });
}